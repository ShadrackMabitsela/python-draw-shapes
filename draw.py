

# TODO: Step 1 - get shape (it can't be blank and must be a valid shape!)
def get_shape():
    shape_found = False
    shape_list = ['pyramid', 'square', 'triangle',
                  'parallelogram', 'trapezoid', '3dcube']
    while shape_found == False:
        user_shape = input("Shape?: ").lower()
        if user_shape == "" or user_shape not in shape_list:
            shape_found = False
        elif user_shape in shape_list:
            shape_found = True
    return user_shape

# TODO: Step 1 - get height (it must be int!)


def get_height():
    user_height = ''
    height_accepted = False
    while height_accepted == False:
        user_height = input("Height?: ")
        if user_height.isdigit():
            user_height = int(user_height)
            if user_height > 80:
                height_accepted = False
            elif user_height <= 80 and user_height > 0:
                height_accepted = True
        else:
            height_accepted = False
    return user_height

# TODO: Step 2


def draw_pyramid(height, outline):
    if outline == False:
        row_space = height - 1
        number_of_stars = 1
        for _ in range(0, height):                  # For Loop for the rows.
            # For Loop for the columbs/spaces.
            for _ in range(0, row_space):
                print(' ', end="")
            # For loop to print the number of '*'.
            for _ in range(0, number_of_stars):
                print("*", end="")
            print()
            row_space -= 1
            number_of_stars += 2
    elif outline == True:
        front_spaces = height - 1
        number_of_stars = 1
        hollow_spaces = 1
        # first row
        for _ in range(1):
            for _ in range(0, front_spaces):
                print(' ', end="")
            print("*")
            front_spaces -= 1
            number_of_stars += 2
        # middle section
        for _ in range(0, height - 2):
            for _ in range(0, front_spaces):
                print(' ', end="")
            print('*', end='')
            for _ in range(0, hollow_spaces):
                print(' ', end="")
            print('*', end='')
            print()
            front_spaces -= 1
            number_of_stars += 2
            hollow_spaces += 2
        # last row
        for _ in range(0, number_of_stars):
            print('*', end='')
        print()


# TODO: Step 3
def draw_square(height, outline):
    if outline == False:
        for _ in range(0, height):      # For Loop for the rows
            for _ in range(0, height):  # For Loop for columbs
                print("*", end="")
            print()
    elif outline == True:
        # Top Row
        for _ in range(0, height):
            print("*", end="")
        print()
        # Middle Section
        for _ in range(0, height - 2):  # For loop for the hollow rows
            print("*", end="")
            for _ in range(0, height - 2):
                print(" ", end="")
            print("*", end="")
            print()
        # Bottom Row
        for _ in range(0, height):
            print("*", end="")
        print()

# TODO: Step 4


def draw_triangle(height, outline):
    if outline == False:
        number_of_stars = 1
        # For Loop for the rows.
        for _ in range(0, height):
            # For loop to print the number of '*'
            for _ in range(0, number_of_stars):
                print("*", end="")
            print()
            number_of_stars += 1
    elif outline == True:
        number_of_stars = 3     # Includes the first two rows
        number_of_spaces = 1
        print("*")              # First Row
        print("**")             # Second Row
        # Middle Section
        for _ in range(0, height - 3):
            print("*", end="")
            for _ in range(0, number_of_spaces):
                print(" ", end="")
            print("*", end="")
            print()
            number_of_spaces += 1
            number_of_stars += 1
        # Last Row
        for _ in range(0, number_of_stars):
            print("*", end="")
        print()


def draw_parallelogram(height, outline):
    if outline == False:
        front_spaces = height - 1
        for _ in range(0, height):
            for _ in range(0, front_spaces):
                print(" ", end="")
            for _ in range(0, height):
                print("*", end="")
            print()
            front_spaces -= 1
    elif outline == True:
        front_spaces = height - 1
        # Top Row
        for _ in range(0, front_spaces):
            print(" ", end="")
        for _ in range(0, height):
            print("*", end="")
        print()
        front_spaces -= 1
        # Middle Section
        hollow_spaces = height - 2
        for _ in range(0, height - 2):
            for _ in range(0, front_spaces):
                print(" ", end="")
            print("*", end="")
            for _ in range(0, hollow_spaces):
                print(" ", end="")
            print("*", end="")
            print()
            front_spaces -= 1
        # Last Row
        for _ in range(0, height):
            print("*", end="")
        print()


def draw_trapezoid(height, outline):
    if outline == False:
        front_spaces = height - 1
        number_of_stars = height
        for _ in range(0, height):      # Rows
            for _ in range(0, front_spaces):
                print(" ", end="")
            for _ in range(0, number_of_stars):
                print("*", end="")
            front_spaces -= 1
            number_of_stars += 2
            print()
    elif outline == True:
        front_spaces = height - 1
        hollow_spaces = height
        number_of_stars = height
        # Top Row
        for _ in range(0, front_spaces):
            print(" ", end="")
        for _ in range(0, number_of_stars):
            print("*", end="")
        print()
        front_spaces -= 1
        number_of_stars += 2
        # Middle Section
        for _ in range(0, height - 2):
            for _ in range(0, front_spaces):
                print(" ", end="")
            print("*", end="")
            for _ in range(0, hollow_spaces):
                print(" ", end="")
            print("*", end="")
            print()
            front_spaces -= 1
            number_of_stars += 2
            hollow_spaces += 2
        # Bottom Row
        for _ in range(0, number_of_stars):
            print("*", end="")
        print()


def draw_3dcube(height, outline):
    if outline == False:
        # Parallelogram Section
        front_spaces = height - 1
        parallelogram_side_triangle = 0
        for _ in range(0, height):
            for _ in range(0, front_spaces):
                print(" ", end="")
            for _ in range(0, height):
                print("-", end="")
            for _ in range(0, parallelogram_side_triangle):
                print("/", end="")
            print()
            front_spaces -= 1
            parallelogram_side_triangle += 1
        # Square Section
        square_side_triangle = parallelogram_side_triangle - 1
        for _ in range(0, height):      # For Loop for the rows
            for _ in range(0, height):  # For Loop for columbs
                print("*", end="")
            for _ in range(0, square_side_triangle):
                print("/", end="")
            print()
            square_side_triangle -= 1
    elif outline == True:
        ### Hollow Parallelogram Section
        front_spaces = height - 1
        # Top Row
        for _ in range(0, front_spaces):
            print(" ", end="")
        for _ in range(0, height):
            print("-", end="")
        print()
        front_spaces -= 1
        # Middle Section
        hollow_spaces = height - 2
        parallelogram_inner_space = 0
        right_angle_triangle_counter = 1
        for _ in range(0, height - 2):
            for _ in range(0, front_spaces):
                print(" ", end="")
            print("-", end="")
            for _ in range(0, hollow_spaces):
                print(" ", end="")
            print("-", end="")
            if right_angle_triangle_counter < 3:
                for _ in range(0, right_angle_triangle_counter):
                    print("/", end="")
            if right_angle_triangle_counter >= 3:
                parallelogram_inner_space += 1
                print("/", end="")
                for _ in range(0, parallelogram_inner_space):
                    print(" ", end="")
                print("/", end="")
            print()
            front_spaces -= 1
            right_angle_triangle_counter += 1
        # Last Row
        for _ in range(0, height):
            print("-", end="")
        parallelogram_inner_space += 1
        print("/", end="")
        for _ in range(0, parallelogram_inner_space):
            print(" ", end="")
        print("/", end="")
        print()

        ### Hollow Square Section
        # Top Row
        for _ in range(0, height):
            print("*", end="")
        print("/", end="")
        for _ in range(0, parallelogram_inner_space):
            print(" ", end="")
        print("/", end="")
        print()
        # Middle Section
        parallelogram_inner_space -= 1
        for _ in range(0, height - 2):  # For loop for the hollow rows
            print("*", end="")
            for _ in range(0, height - 2):
                print(" ", end="")
            print("*", end="")
            print("/", end="")
            for _ in range(0, parallelogram_inner_space):
                print(" ", end="")
            if parallelogram_inner_space >= 0:
                print("/", end="")
            print()
            parallelogram_inner_space -= 1
        # Bottom Row
        for _ in range(0, height):
            print("*", end="")
        print()


# TODO: Steps 2 to 4, 6 - add support for other shapes
def draw(shape, height, outline):
    if shape == 'pyramid':
        draw_pyramid(height, outline)
    elif shape == 'square':
        draw_square(height, outline)
    elif shape == 'triangle':
        draw_triangle(height, outline)
    elif shape == 'parallelogram':
        draw_parallelogram(height, outline)
    elif shape == 'trapezoid':
        draw_trapezoid(height, outline)
    elif shape == '3dcube':
        draw_3dcube(height, outline)


# TODO: Step 5 - get input from user to draw outline or solid
def get_outline():
    accepted_answer = False
    outline_option = False
    while accepted_answer == False:
        user_option = input("Outline only? (y/N): ").lower()
        if user_option == 'n' or user_option == 'y':
            accepted_answer = True
            if user_option == "y":
                outline_option = True
            elif user_option == "n":
                outline_option = False
    return outline_option


if __name__ == "__main__":
    shape_param = get_shape()
    height_param = get_height()
    outline_param = get_outline()
    draw(shape_param, height_param, outline_param)
